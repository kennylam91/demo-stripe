const express = require("express");
const app = express();
const { resolve } = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
app.use(cors());
app.options("*", cors());

// Replace if using a different env file or config
require("dotenv").config({ path: "./.env" });
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

// Use JSON parser for all non-webhook routes.
app.use((req, res, next) => {
  if (req.originalUrl === "/stripe-events") {
    next();
  } else {
    bodyParser.json()(req, res, next);
  }
});

app.get("/config", async (req, res) => {
  res.status(200).send({
    publishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
  });
});

app.post("/create-customer", async (req, res) => {
  // Create a new customer object
  const customer = await stripe.customers.create(req.body);
  res.status(200).send({ customer });
});

app.post("/create-subscription", async (req, res) => {
  // Set the default payment method on the customer
  try {
    await stripe.paymentMethods.attach(req.body.paymentMethodId, {
      customer: req.body.customerId,
    });
  } catch (error) {
    return res.status(402).send({ error: { message: error.message } });
  }

  let updateCustomerDefaultPaymentMethod = await stripe.customers.update(
    req.body.customerId,
    {
      invoice_settings: {
        default_payment_method: req.body.paymentMethodId,
      },
    }
  );
  // Create the subscription
  try {
    const subscription = await stripe.subscriptions.create({
      customer: req.body.customerId,
      items: [
        { price: process.env[req.body.priceId], quantity: req.body.quantity },
      ],
      expand: ["latest_invoice.payment_intent", "plan.product"],
    });
    res.status(200).send({ subscription });
  } catch (error) {
    return res.status("402").send({ error: { message: error.message } });
  }
});

app.post("/retrieve-upcoming-invoice", async (req, res) => {
  const price = process.env[req.body.priceId.toUpperCase()];
  const quantity = req.body.quantity;

  var params = {};
  params["customer"] = req.body.customerId;
  params["subscription_items"] = [
    {
      price: price,
      quantity: quantity,
    },
  ];
  try {
    const invoice = await stripe.invoices.retrieveUpcoming(params);
    res.status(200).send({ invoice: invoice });
  } catch (error) {
    res.status(402).send({ error: { message: error.message } });
  }
  
});

app.post("/pay-onetime", async (req, res) => {
  try {
    const paymentIntent = await stripe.paymentIntents.create({
      amount: req.body.amount,
      currency: process.env["CURRENCY"],
      payment_method: req.body.paymentMethodId,
      customer: req.body.customerId,
    });
    let response = await stripe.paymentIntents.confirm(paymentIntent.id);
    return res.status(200).send(response);
  } catch (error) {
    return res.status(402)
      .send({ result: { error: { message: error.message } } });
  }
});

app.post("/get-price", async (req, res) => {
  const priceId = process.env[req.body.priceId.toUpperCase()];
  console.log(priceId);
  stripe.prices.retrieve(priceId, (error, price) => {
    if (!error) {
      res.status(200).send(price);
    } else {
      res.status(402)
      .send({ error: { message: error.message } });
    }
  });
});

app.post("/retry-invoice", async (req, res) => {
  // Set the default payment method on the customer

  try {
    await stripe.paymentMethods.attach(req.body.paymentMethodId, {
      customer: req.body.customerId,
    });
    await stripe.customers.update(req.body.customerId, {
      invoice_settings: {
        default_payment_method: req.body.paymentMethodId,
      },
    });
  } catch (error) {
    // in case card_decline error
    return res
      .status(402)
      .send({ result: { error: { message: error.message } } });
  }

  const invoice = await stripe.invoices.retrieve(req.body.invoiceId, {
    expand: ["payment_intent"],
  });
  res.status(200).send(invoice);
});

// Webhook handler for asynchronous events.
app.post(
  "/stripe-webhook",
  bodyParser.raw({ type: "application/json" }),
  async (req, res) => {
    // Retrieve the event by verifying the signature using the raw body and secret.
    let event;

    try {
      event = stripe.webhooks.constructEvent(
        req.body,
        req.headers["stripe-signature"],
        process.env.STRIPE_WEBHOOK_SECRET
      );
    } catch (err) {
      console.log(err);
      console.log(`⚠️  Webhook signature verification failed.`);
      console.log(
        `⚠️  Check the env file and enter the correct webhook secret.`
      );
      return res.sendStatus(400);
    }
    // Extract the object from the event.
    const dataObject = event.data.object;

    // Handle the event
    // Review important events for Billing webhooks
    // https://stripe.com/docs/billing/webhooks
    // Remove comment to see the various objects sent for this sample
    switch (event.type) {
      case "invoice.paid":
        // Used to provision services after the trial has ended.
        // The status of the invoice will show up as paid. Store the status in your
        // database to reference when a user accesses your service to avoid hitting rate limits.
        break;
      case "invoice.payment_failed":
        // If the payment fails or the customer does not have a valid payment method,
        //  an invoice.payment_failed event is sent, the subscription becomes past_due.
        // Use this webhook to notify your user that their payment has
        // failed and to retrieve new card details.
        break;
      case "invoice.finalized":
        // If you want to manually send out invoices to your customers
        // or store them locally to reference to avoid hitting Stripe rate limits.
        break;
      case "customer.subscription.deleted":
        if (event.request != null) {
          // handle a subscription cancelled by your request
          // from above.
        } else {
          // handle subscription cancelled automatically based
          // upon your subscription settings.
        }
        break;
      case "customer.subscription.trial_will_end":
        // Send notification to your user that the trial will end
        break;
      default:
      // Unexpected event type
    }
    res.sendStatus(200);
  }
);
const port = process.env.PORT || 8080;

app.listen(port, () => console.log(`Node server listening on port ${8080}!`));
